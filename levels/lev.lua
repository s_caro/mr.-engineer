--sheet for the different levels
local lev = {}

lev[1] = { title = "lev1.png", background = "outside.jpg"}

lev[1].sheet =
    {
        frames = {
        
            {
                -- beer
                x=0,
                y=0,
                width=705,
                height=450,

            },
            {
                -- book
                x=0,
                y=450,
                width=705,
                height=450,

            },
        },
    }

lev[1].frameIndex =
    {

        ["beer"] = 1,
        ["book"] = 2,
    }

lev[2] = { title = "lev2.png", background = "serverfarm.jpg" }

lev[2].sheet =
{
    frames = {
    
        {
            -- bug
            x=0,
            y=432,
            width=462,
            height=432,

        },
        {
            -- c
            x=0,
            y=0,
            width=462,
            height=432,
            
        },
    },
}

lev[2].frameIndex =
{

    ["bug"] = 1,
    ["c"] = 2,
}

lev[3] = { title = "lev3.png", background = "classroom.jpg" }

lev[3].sheet =
{
    frames = {
    
        {
            -- movie
            x=0,
            y=432,
            width=432,
            height=432,
            

        },
        {
            -- laptop
            x=0,
            y=0,
            width=432,
            height=432,

        },
    },
}

lev[3].frameIndex =
{

    ["movie"] = 1,
    ["laptop"] = 2,
}

lev[4] = { title = "lev4.png", background = "bedroom.jpg" }

lev[4].sheet =
{
    frames = {
    
        {
            -- videogame
            x=0,
            y=417,
            width=598,
            height=417,

        },
        {
            -- bulb
            x=0,
            y=0,
            width=598,
            height=417,

        },
    },

}

lev[4].frameIndex =
{

    ["videogame"] = 1,
    ["bulb"] = 2,
}

lev[5] = { title = "lev5.png", background = "outside.jpg" }

lev[5].sheet =
{
    frames = {
    
        {
            -- kettlebell
            x=0,
            y=432,
            width=411,
            height=432,
            

        },
        {
            -- laptop
            x=0,
            y=0,
            width=411,
            height=432,

        },
    },
}

lev[5].frameIndex =
{

    ["kettlebell"] = 1,
    ["laptop"] = 2,
}

lev[6] = { title = "lev6.png", background = "bedroom.jpg" }

lev[6].sheet =
{
    frames = {
    
        {
            -- mum
            x=376,
            y=0,
            width=376,
            height=446,
            

        },
        {
            -- broom
            x=0,
            y=0,
            width=376,
            height=446,

        },
    },
}

lev[6].frameIndex =
{

    ["mum"] = 1,
    ["broom"] = 2,
}

lev[7] = { title = "lev7.png", background = "classroom1.jpg" }

lev[7].sheet =
{
    frames = {
    
        {
            -- prof
            x=0,
            y=450,
            width=705,
            height=450,

        },
        {
            -- book
            x=0,
            y=0,
            width=705,
            height=450,

        },
    },
}

lev[7].frameIndex =
{

    ["prof"] = 1,
    ["book"] = 2,
}

lev[8] = { title="lev8.png", background = "serverfarm.jpg" }

lev[8].sheet =
{
    frames = {
    
        {
            -- explorer
            x=0,
            y=432,
            width=432,
            height=432,

        },
        {
            -- duck
            x=0,
            y=0,
            width=432,
            height=432,

        },
    },
}

lev[8].frameIndex =
{

    ["explorer"] = 1,
    ["duck"] = 2,
}

lev[9] = { title="lev9.png", background = "outside.jpg" }

lev[9].sheet =
{
    frames = {
    
        {
            -- lighting
            x=354,
            y=0,
            width=354,
            height=612,

        },
        {
            -- multimeter
            x=0,
            y=0,
            width=354,
            height=612,

        },
    },
}

lev[9].frameIndex =
{

    ["lighting"] = 1,
    ["multimeter"] = 2,
}

lev[10] = { title="lev10.png", background = "classroom1.jpg" }

lev[10].sheet =
{
    frames = {
    
        {
            -- chrome
            x=0,
            y=432,
            width=573,
            height=432,

        },
        {
            -- ram
            x=0,
            y=0,
            width=573,
            height=432,

        },
    },
}

lev[10].frameIndex =
{

    ["chrome"] = 1,
    ["ram"] = 2,
}

lev[11] = { title="lev11.png", background = "serverfarm.jpg" }

lev[11].sheet =
{
    frames = {
    
        {
            -- error
            x=0,
            y=0,
            width=512,
            height=512,

        },
        {
            -- stackoverflow
            x=0,
            y=512,
            width=512,
            height=512,

        },
    },
}

lev[11].frameIndex =
{

    ["error"] = 1,
    ["stakoverflow"] = 2,
}

lev[12] = { title="lev12.png", background = "classroom1.jpg" }

lev[12].sheet =
{
    frames = {
    
        {
            -- pigreco
            x=0,
            y=0,
            width=216,
            height=216,

        },
        {
            -- 3
            x=72,
            y=288,
            width=72,
            height=72,

        },
    },
}

lev[12].frameIndex =
{

    ["pigreco"] = 1,
    ["3"] = 2,
}

lev[13] = { title="lev13.png", background = "serverfarm.jpg" }

lev[13].sheet =
{
    frames = {
    
        {
            -- windows
            x=0,
            y=0,
            width=641,
            height=640,

        },
        {
            -- ubuntu
            x=100,
            y=700,
            width=441,
            height=520,

        },
    },
}

lev[13].frameIndex =
{

    ["windows"] = 1,
    ["ubuntu"] = 2,
}

lev[14] = { title="lev14.png", background = "bedroom.jpg" }

lev[14].sheet =
{
    frames = {
    
        {
            -- nowifi
            x=0,
            y=0,
            width=747,
            height=512,

        },
        {
            -- book
            x=0,
            y=512,
            width=747,
            height=512,

        },
    },
}

lev[14].frameIndex =
{

    ["nowifi"] = 1,
    ["book"] = 2,
}

lev[15] = { title="lev15.png", background = "classroom.jpg" }

lev[15].sheet =
{
    frames = {
    
        {
            -- error
            x=0,
            y=512,
            width=512,
            height=512,

        },
        {
            -- semicolon
            x=125,
            y=0,
            width=260,
            height=512,

        },
    },
}

lev[15].frameIndex =
{

    ["error"] = 1,
    ["semicolon"] = 2,
}

lev[16] = { title="lev16.png", background = "serverfarm.jpg" }

lev[16].sheet =
{
    frames = {
    
        {
            -- folder
            x=0,
            y=512,
            width=825,
            height=512,

        },
        {
            -- github
            x=100,
            y=0,
            width=625,
            height=512,

        },
    },
}

lev[16].frameIndex =
{

    ["folder"] = 1,
    ["github"] = 2,
}

lev[17] = { title="lev17.png", background = "outside.jpg" }

lev[17].sheet =
{
    frames = {
    
        {
            -- coronavirus
            x=0,
            y=0,
            width=444,
            height=441,

        },
        {
            -- crown
            x=0,
            y=441,
            width=444,
            height=441,

        },
    },
}

lev[17].frameIndex =
{

    ["coronavirus"] = 1,
    ["crown"] = 2,
}

return lev