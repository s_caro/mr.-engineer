--sheet for the character
local characters =
{
    frames =
    {
        {
            --1) semibald
            x = 512,
            y = 0,
            width = 512,
            height = 560,
        },
        {
            --2) hair
            x = 0,
            y = 560,
            width = 512,
            height = 560,
        },
        {
            --3) helmet
            x = 0,
            y = 0,
            width = 512,
            height = 560,
        },
    }
}

return characters