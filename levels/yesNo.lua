local yesNo = {}

--sheet for the arrows that indicate which object the character has to collect and which he has to avoid

yesNo[1] = { title = "yesNo.png"}

yesNo[1].sheet =
    {
        frames = {
        
            {
                -- no
                x=0,
                y=0,
                width=304,
                height=311,

            },
            {
                -- yes
                x=0,
                y=311,
                width=304,
                height=311,

            },
        },
    }

yesNo[1].frameIndex =
    {

        ["no"] = 1,
        ["yes"] = 2,
    }

return yesNo