--sheet for the number of the levels (in the level menu)
local labelLevel =
{
    frames =
    {
        {
            --1) 1
            x = 0,
            y = 0,
            width = 256,
            height = 256,
        },
        {
            --2) 2
            x = 0,
            y = 768,
            width = 256,
            height = 256,
        },
        {
            --3) 3
            x = 256,
            y = 768,
            width = 256,
            height = 256,
        },
        {
            --4) 4
            x = 512,
            y = 768,
            width = 256,
            height = 256,
        },
        {
            --5) 5
            x = 0,
            y = 1024,
            width = 256,
            height = 256,
        },
        {
            --6) 6
            x = 256,
            y = 1024,
            width = 256,
            height = 256,
        },
        {
            --7) 7
            x = 512,
            y = 1024,
            width = 256,
            height = 256,
        },
        {
            --8) 8
            x = 0,
            y = 1280,
            width = 256,
            height = 256,
        },
        {
            --9) 9
            x = 256,
            y = 1280,
            width = 256,
            height = 256,
        },
        {
            --10) 10
            x = 512,
            y = 512,
            width = 256,
            height = 256,
        },
        {
            --11) 11
            x = 256,
            y = 512,
            width = 256,
            height = 256,
        },
        {
            --12) 12
            x = 0,
            y = 512,
            width = 256,
            height = 256,
        },
        {
            --13) 13
            x = 512,
            y = 256,
            width = 256,
            height = 256,
        },
        {
            --14) 14
            x = 256,
            y = 256,
            width = 256,
            height = 256,
        },
        {
            --15) 15
            x = 0,
            y = 256,
            width = 256,
            height = 256,
        },
        {
            --16) 16
            x = 512,
            y = 0,
            width = 256,
            height = 256,
        },
        {
            --17) 17
            x = 256,
            y = 0,
            width = 256,
            height = 256,
        },
        {
            --1) 1 over
            x = 768,
            y = 0,
            width = 256,
            height = 256,
        },
        {
            --2) 2 over
            x = 768,
            y = 768,
            width = 256,
            height = 256,
        },
        {
            --3) 3 over
            x = 1024,
            y = 768,
            width = 256,
            height = 256,
        },
        {
            --4) 4 over
            x = 1280,
            y = 768,
            width = 256,
            height = 256,
        },
        {
            --5) 5 over
            x = 768,
            y = 1024,
            width = 256,
            height = 256,
        },
        {
            --6) 6 over
            x = 1024,
            y = 1024,
            width = 256,
            height = 256,
        },
        {
            --7) 7 over
            x = 1280,
            y = 1024,
            width = 256,
            height = 256,
        },
        {
            --8) 8 over
            x = 768,
            y = 1280,
            width = 256,
            height = 256,
        },
        {
            --9) 9 over
            x = 1024,
            y = 1280,
            width = 256,
            height = 256,
        },
        {
            --10) 10 over
            x = 1280,
            y = 512,
            width = 256,
            height = 256,
        },
        {
            --11) 11 over
            x = 1024,
            y = 512,
            width = 256,
            height = 256,
        },
        {
            --12) 12 over
            x = 768,
            y = 512,
            width = 256,
            height = 256,
        },
        {
            --13) 13 over
            x = 1280,
            y = 256,
            width = 256,
            height = 256,
        },
        {
            --14) 14 over
            x = 1024,
            y = 256,
            width = 256,
            height = 256,
        },
        {
            --15) 15 over
            x = 768,
            y = 256,
            width = 256,
            height = 256,
        },
        {
            --16) 16 over
            x = 1280,
            y = 0,
            width = 256,
            height = 256,
        },
        {
            --17) 17 over
            x = 1024,
            y = 0,
            width = 256,
            height = 256,
        },
    }
}

return labelLevel
