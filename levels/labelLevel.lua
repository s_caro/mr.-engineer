--sheet for the number of the levels (during the game)
local labelLevel =
{
    frames =
    {
        {
            --1) 1
            x = 0,
            y = 0,
            width = 206,
            height = 140,
        },
        {
            --2) 2
            x = 0,
            y = 420,
            width = 206,
            height = 140,
        },
        {
            --3) 3
            x = 206,
            y = 420,
            width = 206,
            height = 140,
        },
        {
            --4) 4
            x = 412,
            y = 420,
            width = 206,
            height = 140,
        },
        {
            --5) 5
            x = 0,
            y = 560,
            width = 206,
            height = 140,
        },
        {
            --6) 6
            x = 205,
            y = 560,
            width = 206,
            height = 140,
        },
        {
            --7) 7
            x = 412,
            y = 560,
            width = 206,
            height = 140,
        },
        {
            --8) 8
            x = 0,
            y = 700,
            width = 206,
            height = 140,
        },
        {
            --9) 9
            x = 206,
            y = 700,
            width = 206,
            height = 140,
        },
        {
            --10) 10
            x = 412,
            y = 280,
            width = 206,
            height = 140,
        },
        {
            --11) 11
            x = 206,
            y = 280,
            width = 206,
            height = 140,
        },
        {
            --12) 12
            x = 0,
            y = 280,
            width = 206,
            height = 140,
        },
        {
            --13) 13
            x = 412,
            y = 140,
            width = 206,
            height = 140,
        },
        {
            --14) 14
            x = 206,
            y = 140,
            width = 206,
            height = 140,
        },
        {
            --15) 15
            x = 0,
            y = 140,
            width = 206,
            height = 140,
        },
        {
            --16) 16
            x = 412,
            y = 0,
            width = 206,
            height = 140,
        },
        {
            --17) 17
            x = 206,
            y = 0,
            width = 206,
            height = 140,
        },
    }
}

return labelLevel
