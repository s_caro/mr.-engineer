local widget = require( "widget" )
-- Require "global" data table
-- This will contain relevant data like the current level, max levels, number of stars earned, etc.
local checkLevel = require( "data.checkLevel" )
local levelNumbers = require('levels.levMenu')
local M = {}

function M.new(event)

    local group = display.newGroup()
    --checking if is the first time that the player goes to the level menu
    if (event ~= nil) then
        local levelUnlocked = event[1]
        local levelScore = event[2]
        checkLevel.newCurrent(levelUnlocked, levelScore)
        checkLevel.newUnlocked()
    else
        local lev = checkLevel.retrieve()
        checkLevel.new(lev)
    end

    --background of the level menu
    local background = display.newImageRect( group, 'images/background/buildingSite.jpg', _WIDTH, _HEIGHT )
    background.x = _X
    background.y = _Y
    background.alpha = 0.7
    M.background = background

    --level scroll
    local levelSelectGroup = widget.newScrollView({
        width = _WIDTH*2/3,
        height = _HEIGHT/2,
        scrollWidth = _WIDTH*2/3,
        scrollHeight = _HEIGHT*3,
        hideBackground = true,
        horizontalScrollDisabled = true
    })
    M.levelSelectGroup = levelSelectGroup
    

    -- setting offset
    local xOffset = _WIDTH/10
    M.xOffset = xOffset
    local yOffset = _WIDTH/16
    M.yOffset = yOffset
    local cellCount = 1
    M.cellCount = cellCount

    local levelNumbSheet = graphics.newImageSheet( 'images/buttonsOver/numTot.png', levelNumbers)
    -- button's table
    local buttons = {}
    M.buttons = buttons

    --level's button
	for i = 1, 17 do
		buttons[i] = widget.newButton({
            id = i,
            sheet = levelNumbSheet,
            defaultFrame = i,
            overFrame = i + 17,
        })
        -- position of the botton in the grid and add to the scrollview
        buttons[i].x = xOffset
        buttons[i].y = yOffset
        buttons[i].width = _WIDTH/10
        buttons[i].height = _WIDTH/10
        levelSelectGroup:insert(buttons[i])

        xOffset = xOffset +  _WIDTH/6
        cellCount = cellCount + 1
        if cellCount > 4 then
            cellCount = 1
            xOffset =  _WIDTH/10
            yOffset = yOffset +  _WIDTH/6
        end
        M.buttons[i] = buttons[i]
    end

    -- load the scrollview into the scene and center it
    group:insert(levelSelectGroup)
    levelSelectGroup.x = _X
    levelSelectGroup.y = _Y

    --button to go back to home menu
    local doneButton = widget.newButton({
        id = "cancel",
        defaultFile = "images/buttons/homeButton.png",
        overFile = "images/buttonsOver/homeButton.png",
        labelColor = {default = {0}, over = {0.5}},
        width = _HEIGHT/4,
        height = _HEIGHT/5,
    })
    doneButton.x = _X
    doneButton.y = _Y/4
    M.doneButton = doneButton
    group:insert( doneButton )

    --insert group
    M.group = group
    return M
end
return M