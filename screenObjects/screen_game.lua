local widget = require( "widget" )
local lev = require('levels.lev')
local objCreate = require('gameObjects.objects')
local levelNumbers = require('levels.labelLevel')
local M = {}

--variables for the buttons
local left
local right
local up
local doneButton

function M.new(gCurrentLevel, parameter)

    local uiGroup = display.newGroup()
    --bar where are displayd lives, score, pause button and level's info
    local upBar = display.newImageRect(uiGroup, 'images/scritte/prova.png', _WIDTH ,_WIDTH/10)
    upBar.x = _X
    upBar.y = _WIDTH/20
    M.upBar = upBar

    --left button 
    left = widget.newButton({
        id = "left",
        width = _X/4,
        height = _X/4,
        x = _WIDTH / 8,
        y = _HEIGHT * (4/5),
        defaultFile = 'images/buttons/transparentDark22.png',
        overFile = 'images/buttons/shadedDark24.png'
    
    })
    uiGroup:insert(left)
    M.left = left

    --right button
    right = widget.newButton({
        id = "right",
        width = _X/4,
        height = _X/4,
        x = _WIDTH * (2/8),
        y = _HEIGHT * (4/5),
        defaultFile = 'images/buttons/transparentDark23.png',
        overFile = 'images/buttons/shadedDark25.png'
    
    })
    uiGroup:insert(right)
    M.right = right

    --jump button
    up = widget.newButton({
        id = "up",
        width = _X/4,
        height = _X/4,
        x = _WIDTH * (7/8),
        y = _HEIGHT * (4/5),
        defaultFile = 'images/buttons/transparentDark24.png',
        overFile = 'images/buttons/shadedDark26.png'
    
    })
    uiGroup:insert(up)
    M.up = up

    --x position of the first lives
    local livesX = _X  / 3 +_WIDTH/13
    local lives = {}
    -- Display lives and score
    for i = 1, 3 do
        lives[i] = display.newImageRect( uiGroup, 'images/lives/life-1.png', _WIDTH/7, _WIDTH/7 )
        lives[i].x = livesX
        lives[i].y = _Y/4
        livesX = livesX + _X/5
    end
    M.lives = lives

    --text of the score
    local scoreText = display.newText( uiGroup, parameter[2] , _X + _X/6, _Y/5 - 5, native.systemFont, _WIDTH/18 )
    scoreText:setFillColor(0,0,0)
    M.scoreText = scoreText

    --pause button
    doneButton = widget.newButton({
        id = "cancel",
        width = _WIDTH/12,
        height = _WIDTH/12,
        x = _X/7 + _WIDTH/13,
        y = _Y/6,
        defaultFile = 'images/buttons/pause.png'
        
    })
    uiGroup:insert( doneButton )
    M.doneButton = doneButton

    --image for the level
    local levelLabel = display.newImageRect(uiGroup, 'images/scritte/level.png', _WIDTH/7, _WIDTH/11 )
    levelLabel.x = _X + _X/2
    levelLabel.y = _Y/6
    M.levelLabel = levelLabel
    --number of the current level
    local levelNumbSheet = graphics.newImageSheet( 'images/buttons/numeri.png', levelNumbers)
    local levelNumb = display.newImageRect(uiGroup, levelNumbSheet, gCurrentLevel, _WIDTH/11,  _WIDTH/11)
    levelNumb.x = _X + _X*3/4
    levelNumb.y = _Y/6
    M.levelNumb = levelNumb
    --insert group
    M.uiGroup = uiGroup
    return M
end

return M