--the objects that will be display in the reload_game scene are created here
local M = {}

local widget = require( "widget" )
local lev = require("levels.lev")
local yesNo = require("levels.yesNo")
local objectSheet
local currentLevel
local yesNoSheet

function M.new(currLev)

    local group = display.newGroup()
    --object displayed on the screen
    objectSheet = graphics.newImageSheet( 'images/objects/lev/' .. lev[currLev].title , lev[currLev].sheet )
    --arrows
    yesNoSheet = graphics.newImageSheet( 'images/objects/' .. yesNo[1].title, yesNo[1].sheet)
    --background for the current scene
    local background = display.newImageRect(group, 'images/background/buildingSite.jpg', _WIDTH, _HEIGHT)
    background.x = _X
    background.y = _Y
    background.alpha = 0.7
    --assign the background to M
    M.background = background

    --button to begin the games
    local continue  = widget.newButton({
        id = "continue",
        defaultFile = 'images/buttons/start.png',
        overFile = "images/buttonsOver/start.png",
        width =  _X*3/4,
        height = _HEIGHT/4,
    })
    continue.x = _X
    continue.y = _HEIGHT * 5/6
    --continue:addEventListener("tap", handleContinueButton)
    group:insert( continue )
    M.continue = continue

    local back  = widget.newButton({
        id = "back",
        defaultFile = 'images/buttons/back.png',
        overFile = "images/buttonsOver/back.png",
        width =  _HEIGHT/5,
        height = _HEIGHT/5,
    })
    back.x = _WIDTH/9
    back.y = _HEIGHT * 5/6
    --back:addEventListener("tap", handleBackButton)
    group:insert( back )
    M.back = back

    --ok sign
    local yes = display.newImageRect ( group, yesNoSheet, 2, _X/4,  _X/4)
    yes.x = _WIDTH*3/4
    yes.y =  _HEIGHT/5
    M.yes = yes

    --green arrow
    local arrowYes = display.newImageRect(group, 'images/objects/arrowgreen.png', _X/4, _Y/4)
    arrowYes.x = _X
    arrowYes.y = _HEIGHT/5
    M.arrowYes = arrowYes

    --good object
    local catchIm = display.newImageRect( group, objectSheet, 2, _X/3,  _Y/3 )
    catchIm.x =  _WIDTH/4
    catchIm.y =  _HEIGHT/5
    M.catchIm = catchIm

    --no sign
    local no = display.newImageRect ( group, yesNoSheet, 1,  _X/4,  _X/4)
    no.x = _WIDTH*3/4
    no.y = _HEIGHT*2/3 - _Y/4
    M.no = no

    --red arrow
    local arrowNo = display.newImageRect(group, 'images/objects/arrowred.png', _X/4,  _Y/4)
    arrowNo.x = _X
    arrowNo.y = _HEIGHT*2/3 - _Y/5
    M.arrowNo = arrowNo

    --bad object
    local avoidIm = display.newImageRect( group, objectSheet, 1, _X/3, _Y/3 )
    avoidIm.x = _WIDTH/4
    avoidIm.y = _HEIGHT*2/3 - _Y/4
    M.avoidIm = avoidIm
    
    --all the objets are insert into the group
    M.group = group
    return M
end
return M