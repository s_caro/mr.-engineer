local widget = require( "widget" )
local checkLevel = require("data.checkLevel")
local myData = require("data.mydata")
--the objects that will be display in the menu scene are created here
local M = {}

function M.new()

    local sceneGroup = display.newGroup()
    --set up terrain and background
    local background = display.newImageRect( sceneGroup, 'images/background/buildingSite.jpg', _WIDTH, _HEIGHT )
    background.x = _X
    background.y = _Y
    background.alpha = 0.7
    M.background = background

    --title of the game
    local title = display.newImageRect(sceneGroup, 'images/scritte/titolo.png', _WIDTH - _WIDTH/16, _HEIGHT*2/5)
    title.x = _X
    title.y = _Y*2/5
    sceneGroup:insert( title )
    M.title = title

    --play button
    local play  = widget.newButton({
        id = "play",
        defaultFile = 'images/buttons/play.png',
        overFile = "images/buttonsOver/play.png",
        width = _WIDTH/3,
        height = _HEIGHT/4,
    })
    sceneGroup:insert(play)
    play.x = _X
    play.y = _Y + _Y/12
    play.name = "play"
    M.play = play


    --login with facebook button
    local login  = widget.newButton({
        id = "login",
        defaultFile = 'images/buttons/facebook.png',
        overFile = "images/buttonsOver/facebook.png",
        width =  _X/4,
        height = _X/4,
    })
    sceneGroup:insert(login)
    login.x = _X
    login.y = _Y + _Y*2/3
    login.name = "login"
    M.login = login

    --credits button
    local credits  = widget.newButton({
        id = "credits",
        defaultFile = 'images/buttons/story.png',
        overFile = "images/buttonsOver/story.png",
        width =  _X/4,
        height = _X/4,
    })
    sceneGroup:insert(credits)
    credits.x = _X - _X/3
    credits.y = _Y + _Y*2/3
    M.credits = credits
    M.sceneGroup = sceneGroup

    --leaderboard button
    local leaderboard  = widget.newButton({
        id = "leaderboard",
        defaultFile = 'images/buttons/coppa.png',
        overFile = "images/buttonsOver/coppa.png",
        width = _X/4,
        height = _X/4,
    })
    sceneGroup:insert(leaderboard)
    leaderboard.x = _X + _X/3
    leaderboard.y =  _Y + _Y*2/3
    M.leaderboard = leaderboard

    --insert group
    M.sceneGroup = sceneGroup
    
    --create table to memorize completed levels if it doesn't exists
    checkLevel.createTable()

    return M
end
return M