local retrieveData = require("data.retrieveData")
local myData = require("data.mydata")

local M = {}

--new level
function M.new(lev)
    M.settings = {}
    M.settings.currentLevel = lev
    M.settings.unlockedLevels = lev
    M.settings.levels = {}
    M.token = nil
end

--setting data for the current level
function M.newCurrent(lev, levelScore)
    M.settings.currentLevel =  lev + 1
    M.settings.levels[lev] = {}
    M.settings.levels[lev].score = levelScore
end

--get highest level completed
function M.retrieve()
    return myData.retrieveUnlocked()
end

--update last level completed
function M.newUnlocked()
    local livello = M.settings.currentLevel
    myData.updateUnlocked(livello)
    M.settings.unlockedLevels = myData.retrieveUnlocked()
end

--create tables if they don't exist
function M.createTable()
    myData.new()
end

return M