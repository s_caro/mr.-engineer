local sqlite3 = require( "sqlite3" )
local retrieveData = require("data.retrieveData")
--setting up local database
local path = system.pathForFile( "data.db", system.DocumentsDirectory )
-- Open "data.db". If the file doesn't exist, it will be created
local db = sqlite3.open( path ) 

local M = {}

function M.insertHighScore(highscore)
    local tablefill = [[INSERT INTO highscore VALUES(date('now'), ]]..highscore..[[);]]
    db:exec(tablefill)
    db:errmsg()
end

--retrieve unlocked levels from local database
function M.retrieveUnlocked()

    for a in db:nrows([[SELECT * FROM livelli;]]) do 
        db:errmsg()
        return a.level
    end
end

--update unlocked levels
function M.updateUnlocked(lev)

    local levXscore = lev -1

    --row counter
    local cont=0
    --higher level played and won
    local currLev
    for a in db:nrows([[SELECT * FROM livelli;]]) do 
        cont = cont +1
        currLev = a.level

    end
    --local and remote database updated
    --case without any presaved data in the database
    if(cont == 0) then
        local tablefill = [[INSERT INTO livelli VALUES(]]..lev..[[);]]
        db:exec(tablefill)
        db:errmsg()
    --cases with datas in the database 
    elseif(lev > currLev and lev > _LIVELLO) then
        local tablefill = [[UPDATE livelli SET level = ]]..lev..[[; ]]
        db:exec(tablefill)
        db:errmsg()
        retrieveData.post(lev)
    elseif(_LIVELLO > lev and _LIVELLO > currLev) then
        local tablefill = [[UPDATE livelli SET level = ]].._LIVELLO..[[; ]]
        db:exec(tablefill)
        db:errmsg()
        retrieveData.post(_LIVELLO)
    end

end

--create new tables for highscore and levels if they don't exist
function M.new()
    local table1setup = [[CREATE TABLE IF NOT EXISTS livelli (level);]]
    db:exec(table1setup)
    db:errmsg()

    local table1setup = [[CREATE TABLE IF NOT EXISTS highscore (data, highscores);]]
    db:exec(table1setup)
    db:errmsg()
end


return M