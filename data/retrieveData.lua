-- Project: Corona json
-- Description: Client to receive data from a MySQL database formatted as json data by php to a local SQLite database.

-- Copyright 2020 Brian Burton. All Rights Reserved.

local sqlite3 = require ("sqlite3")
local myNewData 
local json = require ("json")
local decodedData 
local network = require( "network" )



M = {}

--global variable which contains the value of the current level on the online database 
--(1 is the default value if no level is saved on the database for the current player)
_LIVELLO = 1

--listener of the get request
local function networkListenerGet( event )
     if ( event.isError ) then
          print( "Network error!")
     else
          local t = event.responseHeaders
          myNewData = event.response
          local length = myNewData:len()
          myNewData = myNewData:sub( 2,(length-1) )
          local decFile = json.decode(myNewData)
          if(decFile ~= nil)then
               --assign to _LIVELLO the value retrieved from the online database if a value is retrieved
               _LIVELLO = decFile["livello"]
               _LIVELLO = tonumber(_LIVELLO)
          end
     end

end

--get request
function M.get()
     if(token ~= nil)then
          local url = "https://mrengineer.herokuapp.com/get.php?id="..token

          network.request( url, "GET", networkListenerGet)
     end
end

--function to send data to the database
--(function is called post but is actually a get request)
function M.post(level)
     if(token ~= nil)then
          --listener of the get request to post data
          local function networkListenerPost( event )
               if ( event.isError ) then
                    print( "Network error!")
               else
                    local t = event.responseHeaders
                    myNewData = event.response
                    local length = myNewData:len()
                    myNewData = myNewData:sub( 2,(length-1) )
                    local decFile = json.decode(myNewData)
               end
          end
          
          local url = "https://mrengineer.herokuapp.com/post.php?id="..token.."&lev="..level
          --request to send data to the database
          network.request( url, "GET", networkListenerPost)
     end
end

return M
