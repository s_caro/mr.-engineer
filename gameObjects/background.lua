local composer = require( "composer" )
local lev = require('levels.lev')

local M = {}

local pavement
local background


--background of the game 
function M.new(level)
    
    local group = display.newGroup()
    
    --image of the background
    background = display.newImageRect( 'images/background/' .. lev[level].background, _WIDTH, _HEIGHT )
    background.alpha = 0.7
    background.x = _X
    background.y = _Y
    group:insert(background)

    --solid static body to hold the character
    pavement = display.newRect( group, _X, (_HEIGHT - _HEIGHT/40), _WIDTH*6, _HEIGHT/30)
    pavement.myName = "pavement"
    pavement:setFillColor( 0, 0, 0, 0)
    physics.addBody(pavement, "static" )

    return group
end


return M