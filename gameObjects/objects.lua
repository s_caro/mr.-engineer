local powerup = require('powerUps.powerUps')
local lev = require('levels.lev')


local M = {}
--sheet for the object of the current level
local objectSheet 
local objectsTable = {}
--sheet for the powerUp
local powerUpsSheet 


-- the objects which collide must be removed from the game
function M.removeAfterCollision(obj)

    --it iterates through the table which contains all the objects
    for i = #objectsTable, 1, -1 do
        if ( objectsTable[i] == obj ) then
            table.remove( objectsTable, i )
            break
        end
    end

end


--it creates the objects that the character has to collect
function M.newGood(gCurrentlev)
    
    objectSheet = graphics.newImageSheet( 'images/objects/lev/' .. lev[gCurrentlev].title , lev[gCurrentlev].sheet )
    local good = display.newImageRect( objectSheet, 2, _X/5, _X/6 )
    table.insert( objectsTable, good )
    physics.addBody( good, "dynamic", { radius=_X/12, bounce = 0.3} )
    good.myName = "good"
    if(gCurrentlev/5 > 1)then
        good.gravityScale = 0.1*gCurrentlev/5
    else
        good.gravityScale = 0.1
    end
    good.x = math.random( _WIDTH )
    good.y = -_X/3
    good:setLinearVelocity( math.random( -40, 40 ), math.random( 30, 60 ) )
    return good
    
end

--it creates the objects that the character has to avoid
function M.newBad(gCurrentlev)
    
    objectSheet = graphics.newImageSheet( 'images/objects/lev/' .. lev[gCurrentlev].title , lev[gCurrentlev].sheet )
    local bad = display.newImageRect( objectSheet, 1, _X/5, _X/6 )
    table.insert( objectsTable, bad )
    physics.addBody( bad, "dynamic", { radius=_X/12, bounce = 0.3} )
    bad.myName = "bad"
    if(gCurrentlev/5 > 1)then
        bad.gravityScale = 0.1*gCurrentlev/5
    else
        bad.gravityScale = 0.1
    end
    bad.x = math.random( _WIDTH )
    bad.y = -_X/3
    bad:setLinearVelocity( math.random( -40, 40 ), math.random( 30, 60 ) )
    return bad
    
end

--it creates the object which represents the powerUp
function M.newPow()
    
    powerUpsSheet = graphics.newImageSheet( 'images/objects/powerUps/' .. powerup[1].title , powerup[1].sheet )
    local powerUpObj = display.newImageRect( powerUpsSheet, math.random(1,2), _X/6, _X/6)
    table.insert(objectsTable, powerUpObj)
    physics.addBody(powerUpObj, "dynamic", {radius=_X/12, bounce = 0.3})
    powerUpObj.myName = "powerUp"
    powerUpObj.gravityScale = 0.1
    powerUpObj.x = math.random( _WIDTH )
    powerUpObj.y = -40
    powerUpObj:setLinearVelocity( math.random( -20, 20 ), math.random( 20, 50 ) )
    return powerUpObj
    
end



return M