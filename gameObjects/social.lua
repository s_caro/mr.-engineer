
local M = {}
-- Require libraries/plugins
local json = require( "json" )
local widget = require( "widget" )
local composer = require( "composer" )
local facebook = require( "plugin.facebook.v4a" )
local checkLevel = require("data.checkLevel")


-- State variables for Facebook commands we're executing
local requestedFBCommand
local commandProcessedByFB

-- Facebook commands
local LOGIN = 0

local function valueInTable( t, item )
	for k,v in pairs( t ) do
		if v == item then
			return true
		end
	end
	return false
end


-- Table printing function for Facebook debugging
local function printTable( t )
	print("Printing table", debugOutput )
	if type( t ) ~= "table" then
		print("WARNING: attempt to print a non-table. Table expected, got " .. type( t ) )
		return
	end
	if ( debugOutput == false ) then 
		return 
	end
	print("--------------------------------")
	print( json.prettify( t ) )
end


local function enforceFacebookLoginAndPermissions()
	if facebook.isActive then
		local accessToken = facebook.getCurrentAccessToken()
		if accessToken == nil then
			print( "Need to log in!" )
			facebook.login()
		else
			print( "Already logged in with necessary permissions" )
			printTable( accessToken )
			token = accessToken.userId
			--processFBCommand()
		end
	else
		print( "Please wait for facebook to finish initializing before checking the current access token" );
	end
end


-- New Facebook Connection listener
local function listener( event )

	-- Debug event parameters output
	-- Prints events received up to 20 characters. Prints "..." and total count if longer
	print( "Facebook Listener events:" )
	
	local maxStr = 20		-- set maximum string length
	local endStr
	
	for k,v in pairs( event ) do
		local valueString = tostring(v)
		if string.len(valueString) > maxStr then
			endStr = " ... #" .. tostring(string.len(valueString)) .. ")"
		else
			endStr = ")"
		end
		print( "   " .. tostring( k ) .. "(" .. tostring( string.sub(valueString, 1, maxStr ) ) .. endStr )
	end
	-- End of debug event routine

	print( "event.name", event.name )  -- "fbconnect"
	print( "event.type:", event.type )  -- Type is either "session", "request", or "dialog"
	print( "isError: " .. tostring( event.isError ) )
	print( "didComplete: " .. tostring( event.didComplete ) )
	print( "response: " .. tostring( event.response ) )
	
	-- Process the response to the Facebook command
	-- Note that if the app is already logged in, we will still get a "login" phase
	if ( "session" == event.type ) then

		statusMessage.text = event.phase  -- "login", "loginFailed", "loginCancelled", or "logout"
		print( "Session status: " .. event.phase )

		if ( event.phase ~= "login" ) then
			-- Exit if login error
			return
		else
			-- Run the desired command
			--processFBCommand()
		end

	elseif ( "request" == event.type ) then

		local response = event.response  -- This is a JSON object from the Facebook server

		if ( not event.isError ) then
			response = json.decode( event.response )

			-- Advance the Facebook command state as this command has been processed by Facebook
			commandProcessedByFB = requestedFBCommand

			print( "Facebook Command: " .. commandProcessedByFB )

			if ( commandProcessedByFB == GET_USER_INFO ) then
				statusMessage.text = response.name
				printTable( response )
			else
				statusMessage.text = "(unknown)"
			end
		elseif tostring( event.response ) == "Duplicate status message" then
			statusMessage.text = "Caught duplicate status message!"
			printTable( event.response )
		else
			statusMessage.text = "Post failed"
			printTable( event.response )
		end

	elseif ( "dialog" == event.type ) then
		-- Advance the Facebook command state as this command has been processed by Facebook
		commandProcessedByFB = requestedFBCommand

		statusMessage.text = event.response
	end
end

function M.new (action)
-- Check for an item inside the provided table

	requestedFBCommand = action

	enforceFacebookLoginAndPermissions()

	

end



return M