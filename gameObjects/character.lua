local characters = require('levels.characters')
local charPow = require('powerUps.characterPower')

local M = {}
--sheet for the character in the normal form
local characterSheet = graphics.newImageSheet ( 'images/character/spriteChar1.png', characters )
--variable for the character with the power up
local characterPower
local characterPowerTable = {}
local characterTable = {}
local character


--sequence of sheets for the character with the powerUp
local sequences_charPower = {
    -- consecutive frames sequence
    {
        start = 1,
        count = 3,
        time = 400,
        loopCount = 0,
        loopDirection = "bounce"
    }
}

--it removes the character from the table
function M.remove()
    table.remove(characterTable, 1)
end

--it removes the normal character to disply the character with the powerUp
function M.newCharPow(lives, char)

    --old position of the character
    local oldX = char.x
    local oldY = char.y
    --the function is called to remove the character from the table
    M.remove()
    local characterPowerSheet = graphics.newImageSheet ('images/character/pow/' .. charPow[lives].title, charPow[lives].sheet)

    characterPower = display.newSprite(characterPowerSheet, sequences_charPower)
    table.insert(characterPowerTable, characterPower)
    characterPower.x = oldX
    characterPower.y = oldY
    characterPower.myName="character"
    return characterPower
end

--it removes the character with the powerUp to disply the normal character
function M.removeCharPow(lives, char)

    --old position of the character
    local oldX = char.x
    local oldY = char.y
    table.remove(characterPowerTable, 1)
    character = display.newImageRect( characterSheet, lives, _WIDTH/5, _HEIGHT/4 )
    table.insert( characterTable, character )
    
    character.x = oldX
    character.y = oldY
    
    character.myName = "character"
    return character
end

--it creates the character at the beginning of each level
function M.new(lives)
    
    character = display.newImageRect( characterSheet, lives, _WIDTH/5, _HEIGHT/4 )
    table.insert( characterTable, character )
    
    character.x = _X
    character.y = _HEIGHT - (_HEIGHT/3)
    
    character.myName = "character"


    return character
end

return M