local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()

--function called when the player wants to go back to the main menu
local function handleGoBackButtonEvent()

    composer.gotoScene( "scenes.menu", { effect = "fade", time = 500 } )
    --composer.removeScene('scenes.level_select')

end

--function called when the player wants to read the story of the character
local function handleCredits()

    composer.gotoScene( "scenes.credits", { effect = "fade", time = 500 } )
    --composer.removeScene('scenes.level_select')

end

--create()
function scene:create( event )
    local sceneGroup = self.view

    --background of the scene
    local background = display.newImageRect( sceneGroup, 'images/background/buildingSite.jpg', _WIDTH, _HEIGHT )
    background.x = _X
    background.y = _Y
    background.alpha = 0.7

    --title of the scene
    local title = display.newImageRect( sceneGroup, 'images/scritte/story.png', _WIDTH*2/3, _HEIGHT/3)
    title.x = _X
    title.y = _Y/3

    --home button
    local doneButton = widget.newButton({
        id = "cancel",
        defaultFile = "images/buttons/homeButton.png",
        overFile = "images/buttonsOver/homeButton.png",
        labelColor = {default = {0}, over = {0.5}},
        width = _HEIGHT/6,
        height = _HEIGHT/6,
    })
    doneButton.x = _X - _X/6
    doneButton.y = _HEIGHT*8/9
    doneButton:addEventListener("tap", handleGoBackButtonEvent)
    sceneGroup:insert(doneButton)

    --credits button
    local credits = widget.newButton({
        id = "story",
        defaultFile = "images/buttons/informazioni.png",
        overFile = "images/buttonsOver/informazioni.png",
        labelColor = {default = {0}, over = {0.5}},
        width = _HEIGHT/6,
        height = _HEIGHT/6,
    })
    credits.x = _X + _X/6
    credits.y = _HEIGHT*8/9
    credits:addEventListener("tap", handleCredits)
    sceneGroup:insert(credits)

    local textStory ="This little guy here is a student of computer\nscience and he really wants to be an engineer!\nHelp him finishing all the levels\nso he can finally get his degree!\nBut be careful if he touches the wrong item\nhe will start loosing his hair, if he looses\nall of his hair then he will have to repeat\nthe level and his score will be zero again!\nAt the beginning of each level an arrow will\ntell you which item you need to catch or avoid.\nUse the buttons on the screen to move him!\nHave fun and good luck!"

    local options = {
        text = textStory,
        x = _X*4/5,
        y = _Y,
        width = _WIDTH*2/3 - _X/22,
        height = _HEIGHT,
        fontSize =  _WIDTH/35,
        align = "left"
     }
      
     local textField = display.newText( options )
     textField:setFillColor( 0, 0, 0 )

    --canva where the story is displayed
    local backStory = display.newImageRect(sceneGroup, "images/background/highscoreBack2.png", _WIDTH*4/5, _HEIGHT*3/4 - _HEIGHT*6/20)
    backStory.x = _X
    backStory.y = _Y - _Y/22 + _Y/5

    --highscores scroll
    local storyGroup = widget.newScrollView({
        width = _WIDTH*2/3,
        height = _Y/2 + _Y/4 - _HEIGHT/11,
        top = _Y*2/3 + _Y/5,
        left = _X/5 + _X/12,
        scrollWidth = _WIDTH*2/3,
        scrollHeight = _HEIGHT*10,
        hideBackground = true,
        horizontalScrollDisabled = true
    })
    sceneGroup:insert(storyGroup)
    storyGroup:insert(textField)

end

--show()
function scene:show(event)
    if event.phase == 'will' then

    elseif event.phase == 'did' then
        -- Show it after a moment
        
    end
end

-- hide()
function scene:hide( event )

	local group = self.view
	local phase = event.phase

	if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
    
    end
end


-- destroy()
function scene:destroy( event )

	local group = self.view

end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene)
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene