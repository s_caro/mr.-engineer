local composer = require( "composer" )
local widget = require("widget")
local sqlite3 = require( "sqlite3" )
--setting up local database
local path = system.pathForFile( "data.db", system.DocumentsDirectory )
-- Open "data.db". If the file doesn't exist, it will be created
local db = sqlite3.open( path ) 
local scene = composer.newScene()

--function called when the player wants to go back to the main menu
local function handleGoBackButtonEvent()

    composer.gotoScene( "scenes.menu", { effect = "fade", time = 500 } )
    --composer.removeScene('scenes.level_select')

end

--create()
function scene:create( event )
    local sceneGroup = self.view
    --background of the scene
    local background = display.newImageRect( sceneGroup, 'images/background/buildingSite.jpg', _WIDTH, _HEIGHT )
    background.x = _X
    background.y = _Y
    background.alpha = 0.7

    --title of the scene
    local title = display.newImageRect( sceneGroup, 'images/scritte/highscore.png', _WIDTH*2/3, _HEIGHT*3/8 - _Y*1/20 )
    title.x = _X
    title.y = _Y/4 + _Y/8

    --button to go back to the main menu
    local doneButton = widget.newButton({
        id = "cancel",
        defaultFile = "images/buttons/homeButton.png",
        overFile = "images/buttonsOver/homeButton.png",
        labelColor = {default = {0}, over = {0.5}},
        width = _HEIGHT/6,
        height = _HEIGHT/6,
    })
    doneButton.x = _X
    doneButton.y = _HEIGHT*8/9
    doneButton:addEventListener("tap", handleGoBackButtonEvent)
    sceneGroup:insert(doneButton)

    --canva where the scores are displayed
    local backScore = display.newImageRect(sceneGroup, "images/background/highscoreBack2.png", _WIDTH*4/5, _HEIGHT*3/4 - _HEIGHT*6/20)
    backScore.x = _X
    backScore.y = _Y - _Y/22 + _Y/5

    --highscores scroll
    local levelSelectGroup = widget.newScrollView({
        width = _WIDTH*4/5,
        height = _Y/2 + _Y/4 - _HEIGHT/11,
        top = _Y*2/3 + _Y/5,
        scrollWidth = _WIDTH*2/3,
        scrollHeight = _HEIGHT*3,
        hideBackground = true,
        horizontalScrollDisabled = true
    })
    sceneGroup:insert(levelSelectGroup)


    --table fill with the highscores of the player
    local scoreText = {}
    --table fill with the dates of the highscores of the player
    local dateText = {}
    --counter
    local max = 1
    --space between line
    local dy = 0
    for a in db:nrows([[SELECT * FROM highscore ORDER BY highscores DESC;]]) do 
        if max < 11 then
            --display date of the highscore
            dateText[max] = display.newText (a.data, _X - _X/4 , _WIDTH/20 + dy, native.systemFont, _WIDTH/20 )
            dateText[max]:setFillColor(0,0,0)
            levelSelectGroup:insert(dateText[max])
            --display value of the highscore
            scoreText[max] = display.newText (a.highscores, _X + _X*3/8, _WIDTH/20 + dy, native.systemFont, _WIDTH/20 )
            scoreText[max]:setFillColor(0,0,0)
            levelSelectGroup:insert(scoreText[max])
            --increment counter
            max = max +1
        end
        dy = dy + _WIDTH/16
    end
    --stream music
    backgroundMusic = audio.loadStream( "audio/Intro Theme.mp3" )
end

function scene:show(event)
    if event.phase == 'will' then

    elseif event.phase == 'did' then
        -- Show it after a moment
        
        audio.play( backgroundMusic, { channel=1, loops=-1 } )
    end
end

-- hide()
function scene:hide( event )

	local group = self.view
	local phase = event.phase

	if ( phase == "will" ) then

    elseif ( phase == "did" ) then
        composer.removeScene( "highscore" )
    end
end


-- destroy()
function scene:destroy( event )

	local group = self.view

end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene)
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene