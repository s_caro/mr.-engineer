local composer = require( "composer" )
local widget = require( "widget" )
local facebook = require( "plugin.facebook.v4a" )
local json = require( "json" )
local mydata = require("data.mydata")
local scene = composer.newScene()
local highScore


local requestedFBCommand
local commandProcessedByFB

local SHARE_LINK_DIALOG = 0
local statusMessage = ""
local backgroundMusic


-- Check for an item inside the provided table
local function valueInTable( t, item )
	for k,v in pairs( t ) do
		if v == item then
			return true
		end
	end
	return false
end


-- Table printing function for Facebook debugging
local function printTable( t )
	print("Printing table", debugOutput )
	if type( t ) ~= "table" then
		print("WARNING: attempt to print a non-table. Table expected, got " .. type( t ) )
		return
	end
	if ( debugOutput == false ) then 
		return 
	end
	print("--------------------------------")
	print( json.prettify( t ) )
end


-- Runs the desired Facebook command
local function processFBCommand()

	local response = {}

	-- This displays a Facebook dialog to requests friends to play with you
	if requestedFBCommand == SHARE_LINK_DIALOG then
		-- Create table with link data to share
		local linkData = {
			title = "Mr engineer!",
			name = "Play with me!",
			link = "https://mrengineer.herokuapp.com/share.html",
			caption = "Try to beat my highscore: "..highScore.."!",
			description = "Try to beat my highscore: "..highScore.."!",
			picture = "https://mrengineer.herokuapp.com/htmlShare.png",
		}
		response = facebook.showDialog( "feed", linkData )
	end

	printTable( response )
end 

local function enforceFacebookLoginAndPermissions()
	if facebook.isActive then
		local accessToken = facebook.getCurrentAccessToken()
		if accessToken == nil then
			print( "Need to log in!" )
			facebook.login()
			
		else
			print( "Already logged in with necessary permissions" )
			printTable( accessToken )
			
			processFBCommand()
		end
	else
		print( "Please wait for facebook to finish initializing before checking the current access token" );
	end
end


-- New Facebook Connection listener
local function listener( event )

	-- Process the response to the Facebook command
	-- Note that if the app is already logged in, we will still get a "login" phase
    if ( "session" == event.type ) then

		statusMessage.text = event.phase  -- "login", "loginFailed", "loginCancelled", or "logout"
		print( "Session status: " .. event.phase )

		if ( event.phase ~= "login" ) then
			-- Exit if login error
			return
		else
			
			-- Run the desired command
			processFBCommand()
		end

    elseif ( "request" == event.type ) then

        local response = event.response  -- This is a JSON object from the Facebook server

		if ( not event.isError ) then
	        response = json.decode( event.response )

			-- Advance the Facebook command state as this command has been processed by Facebook
			commandProcessedByFB = requestedFBCommand

			print( "Facebook Command: " .. commandProcessedByFB )

        elseif tostring( event.response ) == "Duplicate status message" then
        	statusMessage.text = "Caught duplicate status message!"
			printTable( event.response )
        else
			statusMessage.text = "Post failed"
			printTable( event.response )
		end

	elseif ( "dialog" == event.type ) then
		-- Advance the Facebook command state as this command has been processed by Facebook
		commandProcessedByFB = requestedFBCommand

		statusMessage.text = event.response
    end
end



local function buttonOnRelease( event )

	local id = event.target.id
	printTable( event )

	if id == "login" then
		requestedFBCommand = LOGIN
        enforceFacebookLoginAndPermissions()
	elseif id == "showRequestDialog" then
		
		requestedFBCommand = SHARE_LINK_DIALOG
		enforceFacebookLoginAndPermissions()
	end
	return true
end

local function goHome(event)
	if( "ended" == event.phase ) then
        composer.gotoScene( "scenes.menu", { effect = "crossFade", time = 500 } )
        composer.removeScene('scenes.shareHighScore')
    end
end


function scene:create( event )

    local group = self.view

    local background = display.newImageRect( group, 'images/background/buildingSite.jpg', _WIDTH, _HEIGHT )
    background.x = _X
    background.y = _Y

    highScore = event.params

	--title of the scene
	local title = display.newImageRect( group, 'images/scritte/gameOver.png', _WIDTH*3/4, _HEIGHT/3)
	title.x = _X
	title.y = _Y/3

	--share title
	local shareTitle = display.newImageRect(group, 'images/scritte/shareTitle.png', _WIDTH/3,_HEIGHT/7)
	shareTitle.x = _X
	shareTitle.y = _Y - _Y/6

       --facebook button
    local shareRequestDialogButton = widget.newButton(
        {
            id = "showRequestDialog",
            x = _X,
            y = _Y + _Y/4,
            width = _HEIGHT/5,
            height = _HEIGHT/5,
			defaultFile = "images/buttons/facebook.png",
			overFile = "images/buttonsOver/facebook.png",
			onRelease = buttonOnRelease
		})

	group:insert( shareRequestDialogButton )
	
	--button to go back to the main menu
	local home = widget.newButton(
		{
			id = "home",
			x = _X,
			y = _HEIGHT*7/8,
			width = _Y/3,
			height = _Y/3,
			defaultFile = "images/buttons/homeButton.png",
			overFile = "images/buttonsOver/homeButton.png",
			onRelease = goHome
		})
	home:addEventListener("tap", goHome)
	group:insert( home )
	
	mydata.insertHighScore(highScore)
	backgroundMusic = audio.loadStream( "audio/Intro Theme.mp3" )
    
end

function scene:show(event)
    if event.phase == 'will' then
        
    elseif event.phase == 'did' then
        -- Show it after a moment
	facebook.init(listener)
	audio.play( backgroundMusic, { channel=1, loops=-1 } )
        
    end
end

-- hide()
function scene:hide( event )

	local group = self.view
    local phase = event.phase
    
    if ( phase == will ) then

        
        --back:removeEventListener("tap", handleBackButton)

	elseif ( phase == "did" ) then
        -- Stop the music!
        --audio.stop( 1 )
        --composer.removeScene( "scenes.reload_game" )
    end
    --print(2)

end


-- destroy()
function scene:destroy( event )

	local group = self.view
	-- Code here runs prior to the removal of scene's view
    -- Dispose audio!
    --audio.dispose( explosionSound )
    --audio.dispose( fireSound )
   -- audio.dispose( musicTrack )
end
    
scene:addEventListener('create')
scene:addEventListener('show')
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
    
return scene