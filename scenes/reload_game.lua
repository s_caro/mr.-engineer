-- This a helper buffer scene.
-- It reloads game scene (game scene can't reload by itself) and shows a loading animation.

local composer = require('composer')

local screenObj = require("screenObjects.screen_reload")

local scene = composer.newScene()

composer.recycleOnSceneChange = true
--parameter passed to the scene
local parameter = {}


--function called when the player decides to continue the game
local function handleContinueButton()
    --current level
    local gCurrentlevel = parameter[1]    
    local score = parameter[2]
    composer.gotoScene('scenes.game', {params ={ gCurrentlevel, score} , time=500, effect="fade"})
    
end

--function called when the player decides to go back to the menu
local function handleBackButton()
    composer.gotoScene("scenes.level_select", {time=500, effect="fade"})
    --composer.removeScene('scenes.reload_game')
end

function scene:create( event )

    local group = self.view
    --parameter passed to the scene
    parameter = event.params
    --current level
    local currLev = parameter[1]
    print("rel "..currLev)
    --screen objects are created
    local screen = screenObj.new(currLev)
    --screen objects are inserted into the disaply group
    group:insert(screen.group)

    --listenerd are added to the buttons
    screen.continue:addEventListener("tap", handleContinueButton)
    screen.back:addEventListener("tap", handleBackButton)
    
    --loading music
    backgroundMusic = audio.loadStream( "audio/Intro Theme.mp3" )

end

function scene:show(event)
    if event.phase == 'will' then
        
    elseif event.phase == 'did' then
        -- Show it after a moment
        --play music
        audio.play( backgroundMusic, { channel=1, loops=-1 } )
    end
end

-- hide()
function scene:hide( event )

	local group = self.view
    local phase = event.phase
    
    if ( phase == will ) then
        --remove all listener
        continue:removeEventListener("tap", handleContinueButton)
        back:removeEventListener("tap", handleBackButton)
	elseif ( phase == "did" ) then
        -- Stop the music!
        audio.stop( 1 )
        composer.removeScene( "reload_game" )
    end
end


-- destroy()
function scene:destroy( event )
	local group = self.view
    --audio.dispose( backgroundMusic )
end


scene:addEventListener('create')
scene:addEventListener('show')
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene
