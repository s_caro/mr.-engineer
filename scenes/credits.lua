local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()

--function called when the player wants to go back to the main menu
local function handleGoBackButtonEvent()

    composer.gotoScene( "scenes.menu", { effect = "fade", time = 500 } )
    --composer.removeScene('scenes.level_select')

end

--function called when the player wants to read the story of the character
local function handleStory()

    composer.gotoScene( "scenes.story", { effect = "fade", time = 500 } )
    --composer.removeScene('scenes.level_select')

end

--create()
function scene:create( event )
    local sceneGroup = self.view

    --background of the scene
    local background = display.newImageRect( sceneGroup, 'images/background/buildingSite.jpg', _WIDTH, _HEIGHT )
    background.x = _X
    background.y = _Y
    background.alpha = 0.7

    --title of the scene
    local title = display.newImageRect( sceneGroup, 'images/scritte/credits1.png', _WIDTH*2/3, _HEIGHT/3)
    title.x = _X
    title.y = _Y/3

    --home button
    local doneButton = widget.newButton({
        id = "cancel",
        defaultFile = "images/buttons/homeButton.png",
        overFile = "images/buttonsOver/homeButton.png",
        labelColor = {default = {0}, over = {0.5}},
        width = _HEIGHT/6,
        height = _HEIGHT/6,
    })
    doneButton.x = _X - _X/6
    doneButton.y = _HEIGHT*8/9
    doneButton:addEventListener("tap", handleGoBackButtonEvent)
    sceneGroup:insert(doneButton)

    --story button
    local story = widget.newButton({
        id = "story",
        defaultFile = "images/buttons/story.png",
        overFile = "images/buttonsOver/story.png",
        labelColor = {default = {0}, over = {0.5}},
        width = _HEIGHT/6,
        height = _HEIGHT/6,
    })
    story.x = _X + _X/6
    story.y = _HEIGHT*8/9
    story:addEventListener("tap", handleStory)
    sceneGroup:insert(story)

    --credits scroll
    local levelSelectGroup = widget.newScrollView({
        width = _WIDTH,
        height = _Y/2 + _Y/4,
        top = _Y*3/4,
        scrollWidth = _WIDTH*2/3,
        scrollHeight = _HEIGHT*3,
        hideBackground = true,
    })
    sceneGroup:insert(levelSelectGroup)

    --creators and contributions
    local credName = display.newImageRect("images/scritte/credits.png", _WIDTH, _HEIGHT)
    credName.x = _X
    credName.y = _Y
    levelSelectGroup:insert(credName)

end

--show()
function scene:show(event)
    if event.phase == 'will' then

    elseif event.phase == 'did' then
        -- Show it after a moment
        
    end
end

-- hide()
function scene:hide( event )

	local group = self.view
	local phase = event.phase

	if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
    
    end
end


-- destroy()
function scene:destroy( event )

	local group = self.view

end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene)
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene