-- Require libraries/plugins
local json = require( "json" )
local composer = require( "composer" )
local logFacebook = require('gameObjects.social')
local screenObj = require("screenObjects.screen_menu")
local retrieveData = require("data.retrieveData")



-- new scene created
local scene = composer.newScene()

--setting global variable for:
--x center position
_X = display.contentCenterX
--y center position
_Y = display.contentCenterY
--width of the screen
_WIDTH = display.contentWidth
--height of the screen
_HEIGHT = display.contentHeight


PIXELW = display.pixelWidth
PIXELH = display.pixelHeight



local backgroundMusic 

--function called when player hits play
local function handlePlay()
    composer.gotoScene( 'scenes.level_select', { effect = "fade", time = 600 })

end

--function called when the player want to facebook login
local function handleLogin()

    logFacebook.new("login")
    retrieveData.get()
    handlePlay()
end

--function called when the player wants to see the credits
local function handleCredits()
        composer.gotoScene( "scenes.story", { effect = "fade", time = 600 })
end

--function called when the player wants to see the leaderborad
local function handleLeaderboard()
    composer.gotoScene( "scenes.highscore", { effect = "fade", time = 600 })
end


--create()
function scene:create( event )
    local sceneGroup = self.view

    local screen = screenObj.new()
    sceneGroup:insert(screen.sceneGroup)
    --listenerd added to buttons
    screen.play:addEventListener("tap", handlePlay)
    screen.login:addEventListener("tap", handleLogin)
    screen.credits:addEventListener("tap", handleCredits)
    screen.leaderboard:addEventListener("tap", handleLeaderboard)
    backgroundMusic = audio.loadStream( "audio/Intro Theme.mp3" )
    --getting the value of the level saved in the online database (if exists)
    

end

--show()
function scene:show(event)
    if event.phase == 'will' then

    elseif event.phase == 'did' then
        -- Show it after a moment
        audio.play( backgroundMusic, { channel=1, loops=-1 } )
        
    end
end

-- hide()
function scene:hide( event )

	local group = self.view
	local phase = event.phase

	if ( phase == "will" ) then

    elseif ( phase == "did" ) then

    end
end


-- destroy()
function scene:destroy( event )

	local group = self.view

end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene)
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene