local composer = require( "composer" )
local widget = require( "widget" )
local lev = require('levels.lev')

local char = require('gameObjects.character')
local powerup = require('powerUps.powerUps')
local back = require('gameObjects.background')
local objCreate = require('gameObjects.objects')
local levelNumbers = require('levels.labelLevel')
local screenObj = require("screenObjects.screen_game")


-- Create a new Composer scene
local scene = composer.newScene()
composer.recycleOnSceneChange = true

local physics = require( "physics" )

physics.start()
--button to go back to the menu
local doneButton
local jumpSound

--group for the objects to display
local backGroup 
local mainGroup 
local uiGroup
--self.view
local sceneGroup

--variable to update the current score
local score

--variable to update the life status of the character
local died

local character

--variable for the different loop of the game
local cont = 0
local contLoopGame = 0

local starAlpha = true
local gameLoopTimer
local scoreText



--table for the lives
local lives = {}
--current number of lives
local nLives

--variable for the powerUp status
--true == no Power Up
--false == Power Up active
local pu = true

--current level
local gCurrentLevel

--variable for the parameter passes to the function
local parameter

local backgroundMusic 
local backgroundMusicPow 

local function updateText()
    scoreText.text = score 
end


--function called when the character ends his lives or the time is finished
local function endGame()
    
    --physics.pause()
    
    if (nLives == 0) then

        composer.gotoScene( "scenes.shareHighScore", { params = score, time=500, effect="fade" } )
        
    
    elseif (gCurrentLevel == 17)then
    
        composer.gotoScene( "scenes.winner", { params = score, time=500, effect="fade" } )

    else

        composer.gotoScene( "scenes.level_select", { params={ gCurrentLevel, score }, time=500, effect="fade" } )
        
    end
end


--function called when the player decides to move the character
local function moveCharacter( event )
    
    local button = event.target.id

    --sound when the player moves
    audio.play(jumpSound)

    if( nLives == 0)  then
        
        timer.performWithDelay( 2000, endGame )
        display.remove( character )
    else

            if(button == "left") then

                transition.to(character, {time=100, x=character.x - _WIDTH/20 } )

            elseif(button == "right" ) then

                transition.to(character, {time=100, x=character.x + _WIDTH/20 } )
            
            elseif( (button == "up") and (character.isBodyActive  == true)) then
                
                character:setLinearVelocity(0,-200)

            end
    end
    -- Prevents touch propagation to underlying objects
    return true 

end


--function called when the character died and he still had lives
local function restoreCharacter(nLives)
    
    --the character is removed from the display and the table
    display.remove( character )
    char.remove()
    
    --new character is created
    character = char.new(nLives)

    mainGroup:insert(character)

    character.isBodyActive = false
    character.alpha = 0
    
    -- Fade in the character
    transition.to( character, { alpha=1, time=300,
        onComplete = function()
            died = false
            character:addEventListener( "collision", onCollision )
            physics.addBody( character, "dynamic", { radius=_HEIGHT/12, friction = 1, density = 4 } )
            character.isBodyActive = true
        end
    } )
    
end

--game loop for the level
local function gameLoop()
    --the counters for the game logic
    --power up counter
    cont = cont + 1
    --gameloop counter
    contLoopGame = contLoopGame + 1

    --if the character has just took the power up then the body must be add to the sheet
    if(pu ~= true and (character.isBodyActive== false))then
        character:addEventListener( "collision", onCollision )
        physics.addBody( character, "dynamic", { radius=_HEIGHT/12, friction = 1, density = 4, bounce=0 } )
        character.isBodyActive = true
    end

    --if the power up is active and the powerup time is over the character must be changed
    if( (cont == 20) and (pu ~= true )) then
        --powerUp music must be stopped
        audio.stop( 3 )
        audio.resume( 2 )
        --powerUp condition set to true
        pu = true
        --remove the listener
        character:removeEventListener( "collision", onCollision )
        character.alpha = 0.5
        --old character removed
        display.remove(character)
        --new character diplayed
        character = char.removeCharPow(nLives, character)
        mainGroup:insert(character)
        --listener added
        character:addEventListener( "collision", onCollision )
        physics.addBody( character, "dynamic", { radius=_HEIGHT/12, friction = 1, density = 4 } )
        character.isBodyActive = true
    end

    --if the time for the level is over go back to the level menu
    if ( contLoopGame == 80) then
        transition.cancel()
        timer.performWithDelay( 300, endGame )
    end 
    
    --on each loop new objects are created 
    local good = objCreate.newGood(gCurrentLevel)
    mainGroup:insert(good)
    local bad = objCreate.newBad(gCurrentLevel)
    mainGroup:insert(bad)
    --power up object are created only each 30 loops
    if (cont == 30) then
        cont = 0
        local powerUp = objCreate.newPow()
        mainGroup:insert(powerUp)
    end

    
end

--function that handles the collision events between the character and the objects
function onCollision( event )
    
    if ( event.phase == "began" ) then

        --object that collided
        local eng = event.target
        local obj = event.other

        if ( obj.myName == "good" )
        then
            -- Remove good object
            display.remove( obj )

            --remove the object from the table
            objCreate.removeAfterCollision(obj)

            -- Increase score
            score = score + 100
            scoreText.text = score
            -- Prevents touch propagation to underlying objects 
            return true
        
        elseif ( obj.myName == "powerUp" ) then
            --pause the normal music

            audio.pause( 2 )
            --play power up music
            audio.play( backgroundMusicPow, { channel=3, loops=-1 } )
            --remove object from the display
            display.remove( obj )
            --remove object fromt the table
            objCreate.removeAfterCollision(obj)
            --remove listener from old character
            character:removeEventListener( "collision", onCollision )
            --setting powerup condition
            pu = false
            character.alpha = 0
            --remove old character from the display
            display.remove( character )
            --display new character
            character = char.newCharPow(nLives, character)
            character.isBodyActive = false
            --scaling the character
            local scaleX = _WIDTH/(5*512)
            local scaleY = _HEIGHT/(4*560)
            character:scale(scaleX, scaleY)
            --powerup effect
            character:play()
            mainGroup:insert(character)
            score = score + 200
            scoreText.text = score
            -- Prevents touch propagation to underlying objects
            return true

        elseif ( obj.myName == "bad" ) then
            --if the powerup isn't active then removes a life
            if(pu) then
                --if the character is alive
                if ( died == false ) then
                    --turnt he character to dead
                    died = true
                    --remove the object from the display
                    display.remove( obj )
                    --remove the object from the table
                    objCreate.removeAfterCollision(obj)
                    -- Update lives
                    display.remove( lives[nLives] )
                    nLives = nLives - 1
                    --check if the character still has lives
                    if nLives == 0 then
                        transition.cancel()
                        timer.performWithDelay( 500, endGame )
                        -- Prevents touch propagation to underlying objects
                        return true
                    else
                        transition.to( character, { alpha=0.1, time=50 } )    
                        timer.performWithDelay( 200, restoreCharacter(nLives))
                        -- Prevents touch propagation to underlying objects
                        return true
                    end
                end
                --if the power up is on just remove the object from the display and from the table
            else
                score = score + 200
                scoreText.text = score
                display.remove( obj )
                objCreate.removeAfterCollision(obj)
                -- Prevents touch propagation to underlying objects
                return true
            end
        end
    end
    return true
end


--collision between pavements and objects
local function collisionPavementObject(event)
    if (((event.object1.myName == "good") or (event.object1.myName == "bad") or (event.object1.myName == "powerUp")) and (event.object2.myName == "pavement")) then
        display.remove(event.object1)
        objCreate.removeAfterCollision(event.object1)
    
    elseif (((event.object2.myName == "good") or (event.object2.myName == "bad") or (event.object2.myName == "powerUp")) and (event.object1.myName == "pavement")) then
        display.remove(event.object2)
        objCreate.removeAfterCollision(event.object2)
    end
end

--return to the game after pause
function scene:resumeGame()
    sceneGroup.alpha = 1
    --start the physics
    physics.start()
    timer.resume( gameLoopTimer )
    return true
end


-- This function is called when the player wants to pause the game
local function handleGoBackButtonEvent( event )
    --physics pause
    physics.pause()
    timer.pause( gameLoopTimer )
    local options = {
        isModal = true,
        effect = "fade",
        time = 200,
    }

    sceneGroup.alpha = 0.5
    --menu to shows overlay
    composer.showOverlay( "scenes.new_game", options )

end

-- This function is called when scene is created
function scene:create( event )

    sceneGroup = self.view
    --paramters of the function
    parameter = event.params
    --current level
    gCurrentLevel = parameter[1]
    
    
    -- Set up display groups
    backGroup = display.newGroup()  -- Display group for the background image
    sceneGroup:insert( backGroup )  -- Insert into the scene's view group
         
    mainGroup = display.newGroup()  -- Display group for the character, objects, , etc.
    sceneGroup:insert( mainGroup )  -- Insert into the scene's view group
         
    uiGroup = display.newGroup()    -- Display group for UI objects like the score
    sceneGroup:insert( uiGroup )    -- Insert into the scene's view group

    --background for the game
    local background = back.new(gCurrentLevel)
    backGroup:insert(background)

    --character created
    character = char.new(3) 
    physics.addBody( character, "dynamic", { radius=_HEIGHT/12, friction = 1, density = 4 } )
    --listener added to the character
    character:addEventListener( "collision", onCollision )
    character.x = _X
    character.y = _HEIGHT - (_HEIGHT/3)
    mainGroup:insert(character)
    --create and insert screen object into the scene
    local screen = screenObj.new(gCurrentLevel, parameter)
    uiGroup:insert(screen.uiGroup)
    --adding listeners
    screen.left:addEventListener("touch", moveCharacter)
    screen.right:addEventListener("touch", moveCharacter)
    screen.up:addEventListener("touch", moveCharacter)
    lives = screen.lives
    scoreText = screen.scoreText
    doneButton = screen.doneButton
    doneButton:addEventListener("tap", handleGoBackButtonEvent)

    --collision listener for the pavement
    Runtime:addEventListener( "collision", collisionPavementObject)

    --loading music 
    backgroundMusic = audio.loadStream( "audio/level.mp3" )
    backgroundMusicPow = audio.loadStream( "audio/power.mp3" )
    jumpSound = audio.loadSound("audio/jump.flac")
    
end

--show()
function scene:show( event )
    
   
	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)
        --setting the variable for the game
        nLives = 3
        score = parameter[2]
        died = false
    end    
    if ( phase == "did" ) then
        
        -- Code here runs when the scene is entirely on screen
        --start physics
        physics.start()
        --start the game loop
        gameLoopTimer = timer.performWithDelay(500, gameLoop, 0)
        -- Start the music!
        audio.play( backgroundMusic, { channel=2, loops=-1 } )
    end   
end

-- hide()
function scene:hide( event )
	local sceneGroup = self.view
    local phase = event.phase
    
	if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
        --canceling the looper
        timer.cancel( gameLoopTimer )
        --stopping physics
        physics.stop()
        --checking if the game is finishing while the character is still alive
        if (nLives > 0) then
            --removing the listener
            character:removeEventListener( "collision", onCollision )
        end
        --removing the listeners
        Runtime:removeEventListener( "collision", collisionPavementObject)
        doneButton:removeEventListener("tap", handleGoBackButtonEvent)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
         
        -- Stop the music!
        audio.stop( 2 )
        audio.stop(3)
    end
    
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view
    -- Dispose audio!
    audio.dispose( jumpSound )
    audio.dispose( backgroundMusic )
    audio.dispose( backgroundMusicPow )
end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )


return scene