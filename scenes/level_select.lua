-- Level Selection Scene
-- Displays a button for each level
local composer = require( "composer" )
local screenObj = require("screenObjects.screen_level")
local checkLevel = require( "data.checkLevel" )

local parameter = {}

local scene = composer.newScene()

--function called when the player wants to go back to the main menu
local function handleGoBackButtonEvent()

    composer.gotoScene( "scenes.menu", { effect = "fade", time = 500 } )
    --composer.removeScene('scenes.level_select')

end

--function called when the player select a level
local function handleLevelSelect( event )
    --if( "ended" == event.phase ) then
        --getting the level selected
        local gCurrentlevel = event.target.id
        print("level"..gCurrentlevel)
        --setting current level
        checkLevel.newCurrent(gCurrentlevel)
        local score
        if(parameter == nil) then
            score = 0
        else
            score = parameter[2]
        end
        
        
        composer.gotoScene( 'scenes.reload_game', { params = { gCurrentlevel, score}, effect = "fade", time = 500 } )
        --composer.removeScene('scenes.level_select')
    --end
end

--create()
function scene:create( event )
    parameter = event.params
    local group = self.view
    local screen = screenObj.new(parameter)
    --screen objects are inserted into the disaply group
    group:insert(screen.group)
    buttons = {}
    for i = 1, 17 do
        buttons[i] = screen.buttons[i]
        --checking whose levels are unlocked
        if ( checkLevel.settings.unlockedLevels == nil ) then
            checkLevel.settings.unlockedLevels = 1
        end
        if ( i <= checkLevel.settings.unlockedLevels ) then
            buttons[i]:setEnabled( true )
            buttons[i].alpha = 1.0
            buttons[i]:addEventListener("tap", handleLevelSelect)
        else 
            buttons[i]:setEnabled( false ) 
            buttons[i].alpha = 0.5 
        end 
        
    end

    screen.doneButton:addEventListener("tap", handleGoBackButtonEvent)
    backgroundMusic = audio.loadStream( "audio/Intro Theme.mp3" )

end

--show()
function scene:show(event)
    if event.phase == 'will' then

    elseif event.phase == 'did' then
        -- Show it after a moment
        audio.play( backgroundMusic, { channel=1, loops=-1 } )
    end
end

-- hide()
function scene:hide( event )

	local group = self.view
	local phase = event.phase

	if ( phase == "did" ) then
        composer.removeScene( "level_select" )
	end
end


-- destroy()
function scene:destroy( event )

	local group = self.view

end
    
scene:addEventListener('create')
scene:addEventListener('show')
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
    
return scene