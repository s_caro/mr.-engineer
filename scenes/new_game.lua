local composer = require( "composer" )
local widget = require("widget") 
local scene = composer.newScene()

local home
local resume

--function called when the player wants to continue the level
local function resumeGame(event)
    -- By some method such as a "resume" button, hide the overlay
    composer.hideOverlay("fade", 200 )
    
end

--function called when the player wants to go back to the main menu
local function homeGame(event)
    -- By some method such as a "resume" button, hide the overlay
    composer.hideOverlay("fade", 200 )
    composer.gotoScene("scenes.menu", { effect = "fade", time = 500 })
    

end

--create()
function scene:create(event)
    local sceneGroup = self.view

    --partial background of the scene
    local sfondo = display.newImageRect(sceneGroup, 'images/background/pause.png',  _WIDTH*2/5, _WIDTH*2/5)
    sfondo.x = _X
    sfondo.y = _Y + _WIDTH/40
    
    --resume button
    resume  = widget.newButton({
        id = "resume",
        defaultFile = 'images/buttons/playButt.png',
        overFile = "images/buttonsOver/playButt.png",
        width =  _WIDTH/8,
        height = _WIDTH/8,
    })
    sceneGroup:insert(resume)
    resume.x = _X
    resume.y = _Y + _WIDTH/40 - _Y/4
    resume:addEventListener("tap", resumeGame)
    
    --home button
    home  = widget.newButton({
        id = "home",
        defaultFile = 'images/buttons/homeButton.png',
        overFile = "images/buttonsOver/homeButton.png",
        width =  _WIDTH/8,
        height = _WIDTH/8,
    })
    sceneGroup:insert(home)
    home.x = _X
    home.y = _Y + _WIDTH/40 + _Y/4
    home:addEventListener("tap", homeGame)
end

--show()
function scene:show(event)
end
 
--hide()
function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase
    local parent = event.parent  -- Reference to the parent scene object
 
    if ( phase == "will" ) then
        resume:removeEventListener("tap", resume)
        home:removeEventListener("tap", home)
        -- Call the "resumeGame()" function in the parent scene
        parent:resumeGame()
    end
end
 

 
scene:addEventListener( "hide", scene )
scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
return scene