local composer = require( "composer" )
local widget = require("widget")
local facebook = require( "plugin.facebook.v4a" )
local json = require( "json" )
local mydata = require("data.mydata")
local social = require("gameObjects.social")
local scene = composer.newScene()
local sceneGroup
local physics = require( "physics" )

physics.start()

--timer to diplay confettis
local confettiLoopTimer
--table to collect confettis
local objectsTable = {}

-- the objects which collide must be removed from the game
local function removeConfetti()

    --it iterates through the table which contains all the objects
    for i = #objectsTable, 1, -1 do
        local thisConfetti =  objectsTable[i]
        if ( thisConfetti.x < -_X/2 or thisConfetti.y > _X*3/2 or thisConfetti.y > _HEIGHT*3/2 ) then
            display.remove (thisConfetti)
            table.remove( objectsTable, i )

        end
    end

end

--function which creates confettis
local function confettiLoop()
    local confetti = display.newImageRect( sceneGroup, "images/objects/winning/coriandoli.png", _X, _Y )
    table.insert( objectsTable, confetti )
    physics.addBody( confetti, "dynamic", { radius=_X/12, bounce = 0.3} )
    confetti.myName = "confetti"
    confetti.gravityScale = 0.1
    confetti.x = math.random( _WIDTH )
    confetti.y = -_X/3
    confetti:setLinearVelocity( math.random( -40, 40 ), math.random( 30, 60 ) )
    removeConfetti()
end


--function to share on facebook the game
local function shareOnFacebook()
    --social.new(SHARE_LINK_DIALOG)
	if facebook.isActive then
		local accessToken = facebook.getCurrentAccessToken()
		if accessToken == nil then
			print( "Need to log in!" )
			facebook.login()
			
        else
            
			print( "Already logged in with necessary permissions" )
			--printTable( accessToken )
			local linkData = {
                title = "Mr engineer!",
                name = "Play with me!",
                link = "https://mrengineer.herokuapp.com/share.html",
                caption = "Try to beat my highscore: "..highScore.."!",
                description = "Try to beat my highscore: "..highScore.."!",
                picture = "https://mrengineer.herokuapp.com/htmlShare.png",
            }
            response = facebook.showDialog( "feed", linkData )
		end
	else
		print( "Please wait for facebook to finish initializing before checking the current access token" );
	end
end

--function called when the player wants to go back to the main menu
local function handleGoBackButtonEvent()

    composer.gotoScene( "scenes.menu", { effect = "fade", time = 500 } )
    timer.performWithDelay(800, composer.removeScene('scenes.winner'), 1)

end

--create()
function scene:create( event )
    sceneGroup = self.view
    highScore = event.params

    --background of the scene
    local background = display.newImageRect( sceneGroup, 'images/background/buildingSite.jpg', _WIDTH, _HEIGHT )
    background.x = _X
    background.y = _Y
    background.alpha = 0.7

    --title of the scene
    local title = display.newImageRect( sceneGroup, 'images/scritte/winner.png', _WIDTH*2/3, _HEIGHT/3)
    title.x = _X
    title.y = _Y/3

    --"now you are an engineer" title
    local eng = display.newImageRect ( sceneGroup, 'images/scritte/finalWrite.png', _WIDTH/2, _HEIGHT/2)
    eng.x = _X
    eng.y = _Y + _Y/6
    

    --button to go back to the main menu
    local doneButton = widget.newButton({
        id = "cancel",
        defaultFile = "images/buttons/homeButton.png",
        overFile = "images/buttonsOver/homeButton.png",
        labelColor = {default = {0}, over = {0.5}},
        width = _HEIGHT/6,
        height = _HEIGHT/6,
    })
    doneButton.x = _X - _HEIGHT/6
    doneButton.y = _HEIGHT*8/9
    --adding listener to trigger the function when the button is tapped
    doneButton:addEventListener("tap", handleGoBackButtonEvent)
    sceneGroup:insert(doneButton)

    --button to share on facebook
    local shareRequestDialogButton = widget.newButton(
        {
            id = "showRequestDialog",
            x = _X + _HEIGHT/6,
            y = _HEIGHT*8/9,
            width = doneButton.width,
            height = doneButton.height,
            defaultFile = "images/buttons/facebook.png",
            overFile = "images/buttonsOver/facebook.png",
            onRelease = shareOnFacebook
        })
    sceneGroup:insert( shareRequestDialogButton )

    --insert highscore in local table
    mydata.insertHighScore(highScore)
    --stream background music
    backgroundMusic = audio.loadStream( "audio/Intro Theme.mp3" )

end

--show()
function scene:show(event)
    if event.phase == 'will' then

    elseif event.phase == 'did' then
        -- Show it after a moment
        
        confettiLoopTimer = timer.performWithDelay(400, confettiLoop, 0)
        audio.play( backgroundMusic, { channel=1, loops=-1 } )
    end
end

-- hide()
function scene:hide( event )

	local group = self.view
	local phase = event.phase

	if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
        timer.cancel( confettiLoopTimer )
        --stopping physics
        physics.stop()
        
        

    elseif ( phase == "did" ) then
        composer.removeScene( "winner" )

    end
end


-- destroy()
function scene:destroy( event )

	local group = self.view
    -- Dispose audio!
   audio.dispose( backgroundMusic )
end

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene)
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene