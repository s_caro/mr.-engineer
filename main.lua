-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

local composer = require( "composer" )



 
-- Seed the random number generator
math.randomseed( os.time() )
-- Removes status bar on iOS
-- https://docs.coronalabs.com/api/library/display/setStatusBar.html
display.setStatusBar( display.HiddenStatusBar ) 

-- Reserve channel 1 for background menu music
audio.reserveChannels( 1 )
-- Reduce the overall volume of the channel
audio.setVolume( 0.5, { channel=1 } )

-- Reserve channel 2 for background game music
audio.reserveChannels( 2 )
-- Reduce the overall volume of the channel
audio.setVolume( 1.5, { channel=2 } )

-- Reserve channel 3 for game powerUp music
audio.reserveChannels( 3 )
-- Reduce the overall volume of the channel
audio.setVolume( 1.5, { channel=3 } )

-- Removes bottom bar on Android 
if system.getInfo( "androidApiLevel" ) and system.getInfo( "androidApiLevel" ) < 19 then
	native.setProperty( "androidSystemUiVisibility", "lowProfile" )
else
	native.setProperty( "androidSystemUiVisibility", "immersiveSticky" ) 
end

-- Go to the menu screen
composer.gotoScene( "scenes.menu" )
