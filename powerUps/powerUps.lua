local powerUp = {}

powerUp[1] = { title = "powerUp.png"}

powerUp[1].sheet =
    {
        frames = {
        
            {
                -- key
                x=0,
                y=0,
                width=505,
                height=470,

            },
            {
                -- coffe
                x=0,
                y=470,
                width=505,
                height=470,

            },
        },
    }

powerUp[1].frameIndex =
    {

        ["key"] = 1,
        ["coffe"] = 2,
    }

return powerUp